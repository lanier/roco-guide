# RocoGuide
洛克王国指南，本项目**所有数据**均来自网络。  
- 点击前往 [Goland编写的服务端](https://github.com/taxeric/RocoGuideServe)  
- 点击前往 [VSCode编写的服务端](https://gitee.com/BaiAnBA/raiders_service)
- 点击前往 [洛克王国指南数据库](https://github.com/taxeric/RocoGuideSql)

## 项目背景
调试新技术 & 未学习的技术。

## 项目提供
本项目提供相关数据及接口，具体实现由您自由编写。  
提供的接口列表：  
+ 日常相关  
   - [x] 每周情报
+ 精灵相关
   - [x] 精灵列表
   - [x] 精灵详情
   - [x] 搜索精灵
   - [x] 技能列表
   - [x] 搜索技能
   - [x] 组别信息
   - [x] 属性信息
   - [x] 天气环境
   - [x] 异常状态
   - [x] 血脉
   - [x] 系别
+ 其他
   - [x] 遗传图鉴
   - [ ] ~~场景及BGM~~ 由独立项目实现


...  
以上接口在数据表均含有相对应的字段，且**未来一段时间内会持续完善实体。**

## 项目数据
- [洛克王国](https://17roco.qq.com/)
- [百度百科](https://baike.baidu.com/item/%E6%B4%9B%E5%85%8B%E7%8E%8B%E5%9B%BD%E5%AE%A0%E7%89%A9%E5%A4%A7%E5%85%A8/4962564)
- [4399技能查询](http://news.4399.com/luoke/jinengsearch/)
- [洛克王国论坛](https://17roco.gamebbs.qq.com/)
- 洛克王国官方公众号

遗传蛋蛋实现请参见: [Roco](https://github.com/taxeric/Roco/)  
更多数据详情请参见: [getResources](https://gitee.com/lanier/roco-guide/blob/master/getResources.md)

## 客户端实现
- [Compose实现的洛克王国指南](https://github.com/taxeric/RocoGuide)
   - gitee直链[下载体验](https://gitee.com/lanier/roco-guide/releases/tag/android_v_2.1)
- [洛克王国BGM](https://github.com/taxeric/RocoBGM)

## 有疑问?
若对数据存疑或想添加其他接口请联系以下账号（**绿色软件**）
- LBA2460
- Eric1248389474

## 测试名单
- [bbyyxx2](https://github.com/bbyyxx2/)
- [XiaoAnXA](https://github.com/XiaoAnXA/)
- [taxeric](https://github.com/taxeric/)
- love****fe61(惊鸿照影)
- shen****i111(琳)
- [JinYucc](https://gitee.com/JinYucc)

## 鸣谢
- [XiaoAnXA](https://github.com/XiaoAnXA/),为本项目提供原始接口支持

## 注
待完善
